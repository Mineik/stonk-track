package StonkTrack.connector.config;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ConfigManagerTest {
	private ConfigManager confM;

	ConfigManagerTest() {
		this.confM = new ConfigManager();
	}

	@Test
	@Order(1)
	void addConfigItem() {
		confM.addConfigItem("testing", "passing");
		var res = confM.gatekeeper.find(KeyValueConfigStore.class, "testing");
		System.out.printf("%s - %s", res.key, res.value);
		assertNotNull(res);
		assertEquals("passing", res.value);
	}

	@Test
	@Order(2)
	void getConfigValue() {
		var res = this.confM.getConfigValue("testing");
		assertEquals("passing", res);
	}

	@Test
	@Order(3)
	void updateConfigValue() {
		this.confM.updateConfigValue("testing", "ok");
		var res = this.confM.gatekeeper.find(KeyValueConfigStore.class, "testing");
		assertNotNull(res);
		assertEquals("ok", res.value);
	}

	@AfterAll
	static void tearDown() {
		new ConfigManager().gatekeeper.createNativeQuery("DROP TABLE config_tbl");
	}
}