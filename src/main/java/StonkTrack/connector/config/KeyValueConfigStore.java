package StonkTrack.connector.config;
import javax.persistence.*;

@Entity
@Table(name = "config_tbl")
public class KeyValueConfigStore {
	@Id @Column(nullable = false)
	public String key;
	@Column(nullable = false)
	public String value;
}
