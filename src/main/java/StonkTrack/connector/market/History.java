package StonkTrack.connector.market;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "market_history")
public class History {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected int id;
	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	protected Date timestamp;
	@Column
	protected double price;
	@ManyToOne
	@JoinColumn(name = "market_token_id", referencedColumnName = "token")
	protected Token token;

	public void setToken(Token token){
		this.token = token;
	}

	public void setTimestamp(Date timestamp){
		this.timestamp = timestamp;
	}

	public void setPrice(double price){
		this.price = price;
	}
}
