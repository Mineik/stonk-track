package StonkTrack.connector.market;
import com.google.gson.Gson;

import javax.persistence.*;

@Entity
@Table(name="token_market")
public class Token {
	@Id @Column(unique = true, nullable = false, length = 10)
	private String token;
	@Column(name="name")
	private String tokenName;

	@Column(name="metadata")
	private String meta;

	@OneToMany(mappedBy = "token", cascade = CascadeType.ALL)
	@OrderColumn(name="id")
	private History[] priceHistory;


	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token.toUpperCase();
	}

	public void setTokenName(String name){
		this.tokenName = name;
	}

	public void setMeta(Object o){
		this.meta = new Gson().toJson(o);
	}

	public History[] getPriceHistory() {
		return this.priceHistory;
	}

	public String toString(){
		return String.format("Market token %s - %s", this.token, this.tokenName);
	}

	public String getTokenName() {
		return tokenName;
	}
}