package StonkTrack.connector;

import StonkTrack.connector.err.HttpRequestException;
import StonkTrack.connector.iface.IRequestCallback;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * Basic connector for HTTP communication
 */
public abstract class HttpConnector {
	protected final HttpClient httpClient;
	private final Gson gson;

	public HttpConnector() {
		this.httpClient = HttpClient.newHttpClient();
		this.gson = new Gson();

	}

	/**
	 * Executes HTTP request, updates schema and returns Object result
	 * @param request request to execute
	 * @param tClass class to create from return
	 * @param callback callback that modifies data
	 * @param <T> type to return
	 * @return type object
	 * @throws IOException oopsie woopsie
	 * @throws InterruptedException fucky wucky
	 * @throws HttpRequestException If your request fails, you will know about it
	 */
	protected <T> T onExecuteRequest(HttpRequest request, Class<T> tClass, IRequestCallback<JsonObject, JsonElement> callback) throws IOException, InterruptedException, HttpRequestException {
		var result = this.httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		if (result.statusCode() < 400) {
			var parsable = callback.item(JsonParser.parseString(result.body()).getAsJsonObject());
			return gson.fromJson(parsable, tClass);
		} else {
			throw new HttpRequestException("Request resulted in error", result);
		}
	}

	public abstract HttpRequest.Builder createBaselineRequest();


	protected HttpRequest.Builder addAppSpecificHeaders(HttpRequest.Builder builder) {
		return builder
				.setHeader("accept", "application/json")
				.setHeader("User-Agent", "StonkTrack");
	}
}
