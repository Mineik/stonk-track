package StonkTrack.connector.err;

import java.net.http.HttpResponse;

public class HttpRequestException extends Exception {
	protected HttpResponse response;

	public HttpRequestException(String message, HttpResponse response) {
		super(message);
		this.response = response;
	}

	public HttpResponse getResponse() {
		return response;
	}
}
