package StonkTrack.ui.components.pages.common;

import StonkTrack.connector.iface.ITodbEntity;

public interface ISaveTab<T, V extends ITodbEntity<T>> {
	public void saveTab(T token);
	public void saveTab(V token);
}
