package StonkTrack.connector.httpResults.finance;

import StonkTrack.connector.iface.ITodbEntity;
import StonkTrack.connector.market.History;
import com.google.gson.annotations.SerializedName;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class ClosePriceSymbol implements ITodbEntity<History> {
	@SerializedName("c")
	public double close;
	@SerializedName("h")
	public double highest;
	@SerializedName("l")
	public double lowest;
	@SerializedName("o")
	public double open;
	@SerializedName("v")
	public double volume;
	@SerializedName("vw")
	public double weightedVolumePrive;
	@SerializedName("t")
	public long unixTimestamp;

	public LocalDate getFromTimestampDate(){
		return Instant.ofEpochMilli(this.unixTimestamp).atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public String getFormatedDate(){
		var dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return dateFormatter.format(this.getFromTimestampDate());
	}

	@Override
	public History toDbEntity() {
		var historyItem = new History();

		historyItem.setPrice(this.close);
		historyItem.setTimestamp(Date.from(this.getFromTimestampDate().atStartOfDay(ZoneId.systemDefault()).toInstant()));

		return historyItem;
	}
}
