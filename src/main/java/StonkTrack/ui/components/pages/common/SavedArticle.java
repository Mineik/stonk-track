package StonkTrack.ui.components.pages.common;

import StonkTrack.ui.components.BaseTab;

import javax.swing.*;
import java.awt.*;

public abstract class SavedArticle extends BaseTab {
	@Override
	public JComponent getComponent() {
		var layout = new JPanel();
		layout.setLayout(new GridBagLayout());
		return layout;
	}

	@Override
	public abstract void loadTabData();
}
