package StonkTrack.ui;
import StonkTrack.ui.components.MarketTokensTab;

import javax.swing.*;

public class ApplicationLayout {

	public static JComponent createMainApplicationFrameLayout(){
		var tabs = new JTabbedPane();
		tabs.addTab("Stonks", new MarketTokensTab().getComponent());

		return tabs;
	}
}
