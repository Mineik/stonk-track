package StonkTrack.connector.httpResults.finance;

import StonkTrack.connector.iface.ITodbEntity;
import StonkTrack.connector.market.Token;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

public class Ticker implements ITodbEntity<Token> {
	public String ticker;
	public String name;
	public String description;
	@SerializedName("homepage_url")
	public String homepageUrl;

	@Override
	public Token toDbEntity() {
		var tok = new Token();
		var gson = new Gson();
		tok.setToken(this.ticker);
		tok.setTokenName(this.name);

		JsonObject obj = new JsonObject();
		obj.add("description", gson.toJsonTree(description));
		obj.add("homepage", gson.toJsonTree(homepageUrl));

		tok.setMeta(obj);

		return tok;
	}

	public String toString(){
		return new Gson().toJson(this);
	}
}
