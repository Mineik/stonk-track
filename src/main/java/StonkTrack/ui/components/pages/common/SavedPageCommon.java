package StonkTrack.ui.components.pages.common;

import javax.swing.*;

public interface SavedPageCommon {
	public JComponent getComponent();
	public void loadTabData();
}
