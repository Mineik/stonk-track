package StonkTrack.ui.components.pages.market;

import StonkTrack.connector.PolygonIoConnector;
import StonkTrack.connector.err.HttpRequestException;
import StonkTrack.connector.httpResults.finance.ClosePriceSymbol;
import StonkTrack.connector.httpResults.finance.Ticker;
import StonkTrack.connector.market.Token;
import StonkTrack.ui.components.BaseTab;
import StonkTrack.ui.components.Items.ChartChart;
import StonkTrack.ui.components.pages.common.ISaveTab;
import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.icons.FlatFileViewFloppyDriveIcon;
import com.formdev.flatlaf.icons.FlatSearchIcon;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.Arrays;

public class Search extends BaseTab implements ActionListener, KeyListener {
	private JTextField textField;
	private JComponent tabComponent;
	private ISaveTab<Token, Ticker> saveTab;

	private ChartChart charter;

	public Search(ISaveTab<Token, Ticker> tabsaver) {
		super();
		this.saveTab = tabsaver;
	}

	@Override
	public JComponent getComponent() {
		var layout = new JPanel();
		layout.setLayout(new GridBagLayout());

		if (this.textField == null) {
			this.textField = new JTextField(20);
			this.textField.putClientProperty(FlatClientProperties.TEXT_FIELD_LEADING_ICON, new FlatSearchIcon());
			this.textField.setFocusable(true);
			this.textField.addKeyListener(this);
		}
		var button = new JButton("Search");
		button.addActionListener(this);

		layout.add(this.textField);
		layout.add(button);
		this.tabComponent = layout;
		return layout;
	}


	private void search() {
		var lookupText = this.textField.getText().toUpperCase();
		var constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.FIRST_LINE_START;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.insets = new Insets(25, 25, 25, 25);
		this.tabComponent.setPreferredSize(new Dimension(1280, 720));


		//TODO search
		Ticker ticker = null;
		ClosePriceSymbol[] history = null;
		try {
			PolygonIoConnector polygonConnector = new PolygonIoConnector("https://api.polygon.io");
			ticker = polygonConnector.queryTickerInfo(lookupText);

			if (ticker != null) {
				System.out.println(ticker);
			}

			history = polygonConnector.queryAllData(lookupText);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (HttpRequestException requestErr) {
			System.out.printf("HTTP request failed with statuscode %d, here is the body: %s (%s)%n", requestErr.getResponse().statusCode(), requestErr.getResponse().body(), requestErr.getResponse().request().uri());
		}

		this.tabComponent.removeAll();
		this.tabComponent.add(new JLabel(ticker != null ? String.format("%s ($%s)", ticker.name, lookupText) : lookupText), constraints);
		constraints.anchor = GridBagConstraints.CENTER;

		if (history != null) {

			constraints.anchor = GridBagConstraints.LINE_START;
			constraints.gridy = 2;
			constraints.gridx = 0;
			constraints.fill = GridBagConstraints.BOTH;
			constraints.weightx = 1.5;
			constraints.weighty = 2;


			var xd = Arrays.stream(history).map(ClosePriceSymbol::getFormatedDate).toArray();

			if (charter == null) {
				charter = new ChartChart();
				charter.createPanel(this.tabComponent,
						Arrays.copyOf(xd, xd.length, String[].class),
						Arrays.stream(history).mapToDouble((ClosePriceSymbol sy) -> sy.close).toArray(),
						constraints
				);
			} else {
				charter.updateChartData(Arrays.copyOf(xd, xd.length, String[].class),
						Arrays.stream(history).mapToDouble((ClosePriceSymbol sy) -> sy.close).toArray());
			}
			constraints.fill = GridBagConstraints.VERTICAL;
			constraints.weighty = 1;
			constraints.weightx = 1;
		}


		// Add search buttons back again
		constraints.anchor = GridBagConstraints.FIRST_LINE_END;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 3;
		constraints.gridy = 0;
		constraints.weightx = 0.25;
		constraints.insets = new Insets(25, 0, 25, 0);
		this.tabComponent.add(this.textField, constraints);
		var button = new JButton("Search");
		button.addActionListener(this);
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.fill = GridBagConstraints.NONE;
		constraints.insets = new Insets(25, 0, 25, 25);
		this.tabComponent.add(button, constraints);
		var saveButton = new JButton(new FlatFileViewFloppyDriveIcon());
		Ticker finalTicker = ticker;
		Ticker finalTicker1 = ticker;
		ClosePriceSymbol[] finalHistory = history;
		saveButton.addActionListener((ActionEvent e) -> {
			if (finalTicker != null) {
				this.saveTab.saveTab(finalTicker);

				this.saveHistory(finalTicker1, finalHistory);
			}
		});

		this.tabComponent.add(saveButton);
		this.textField.setText(ticker != null ? "" : lookupText);
		this.tabComponent.revalidate();
		this.textField.requestFocus();

	}

	protected void saveHistory(Ticker ticker, ClosePriceSymbol[] history) {
		new Thread(() -> {
			var em = Persistence.createEntityManagerFactory("StonkTrack.connector.market").createEntityManager();

			var q = em.createQuery("select t from Token t where t.token=:tokenTicker", Token.class);
			q.setParameter("tokenTicker", ticker.ticker);
			q.setMaxResults(1);

			var rs = q.getResultList().get(0);

			System.out.println(rs.getToken());

			int counter = 0;

			if (rs != null) {
				for (ClosePriceSymbol item : history) {
					counter++;
					System.out.printf("saving item %d/%d%n", counter, history.length);
					var histo = item.toDbEntity();
					histo.setToken(rs);

					em.persist(histo);
					em.getTransaction().begin();
					em.flush();
					em.getTransaction().commit();
					System.out.println("Done");
				}

			}
		}).start();
	}

	@Override
	public void loadTabData() {
	}

	@Override
	protected void objectSetup() {
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.search();
	}


	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER) this.search();
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}
}
