package StonkTrack.connector.config;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.Persistence;

public class ConfigManager {
	EntityManager gatekeeper;
	public ConfigManager(){
		var em = Persistence.createEntityManagerFactory("StonkTrack.connector.config");
		this.gatekeeper = em.createEntityManager();
		this.gatekeeper.setFlushMode(FlushModeType.AUTO);
	}

	public void addConfigItem(String key, String value){
		var item = new KeyValueConfigStore();
		item.key = key;
		item.value = value;

		var transaction = this.gatekeeper.getTransaction();
		transaction.begin();
		this.gatekeeper.persist(item);
		try{
			this.gatekeeper.flush();
			transaction.commit();
		}catch (Exception ignored){
			transaction.rollback();
		}
	}

	public String getConfigValue(String key){
		var item = this.gatekeeper.find(KeyValueConfigStore.class, key);
		return item.value;
	}

	public void updateConfigValue(String key, String value){
		var item = this.gatekeeper.find(KeyValueConfigStore.class, key);
		item.value = value;

		var transaction = this.gatekeeper.getTransaction();
		transaction.begin();
		this.gatekeeper.persist(item);
		try{
			this.gatekeeper.flush();
			transaction.commit();
		}catch(Exception ignored){
			transaction.rollback();
		}
	}
}
