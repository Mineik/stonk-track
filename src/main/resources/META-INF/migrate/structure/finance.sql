create table if not exists `token_market`(`token` varchar(10) not null primary key, `name` not null default '', `metadata` text not null);

create table if not exists `market_history`(`id` integer primary key autoincrement,`timestamp` text not null default current_timestamp,`price` real not null,`market_token_id` varchar(10),foreign key(`market_token_id`) references `token_market`(`token`) on delete cascade);

