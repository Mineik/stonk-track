package StonkTrack.ui.components;
import javax.swing.*;

public abstract class BaseTab {
	public abstract JComponent getComponent();
	public abstract void loadTabData();
	protected abstract void objectSetup();

	public BaseTab(){
		this.objectSetup();
		this.loadTabData();
	}
}
