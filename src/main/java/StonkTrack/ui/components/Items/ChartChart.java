package StonkTrack.ui.components.Items;

import com.google.gson.Gson;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.chart.*;

import javax.swing.*;
import java.awt.*;

public class ChartChart {
	protected LineChart lineChart;
	protected XYChart.Series series;
	public JFXPanel createPanel(JComponent layout, String[] labels, double[] prices, GridBagConstraints constraints) {
		var jfx = new JFXPanel();
		var json = new Gson();

		layout.add(jfx, constraints);

		Platform.runLater(() -> {

			var axisX = new CategoryAxis();
			var axisY = new NumberAxis();
			var chart = new LineChart<String, Number>(axisX, axisY);

			var series = new XYChart.Series<String, Number>();

			System.out.printf("Rendering chart with %d labels and %d price points", labels.length, prices.length);

			for (int i=0; i< prices.length;++i){
				series.getData().add(new XYChart.Data<String, Number>(labels[i], prices[i]));
			}

			chart.getData().add(series);
			jfx.setScene(new Scene(chart));

			this.series = series;
			this.lineChart = chart;

		});
		return jfx;
	}


	public void updateChartData(String[] labels, double[] prices){

		Platform.runLater(()->{
			for (int i=0; i< prices.length;++i){
				this.lineChart.getData().set(i, new XYChart.Data<String, Number>(labels[i],prices[i]));
			}

			if(this.lineChart.getData().size() > prices.length){
				this.lineChart.getData().remove(prices.length, this.lineChart.getData().size()-1);
			}
		});

	}

}
