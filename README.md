## StonkTrack

track whatever asset you basically want, for what I care

## The obligatory how

### The stack

* Java (Developed on JDK 16)
* Gradle (Superior)
* Swing + FlatLaf L&F
* Some APIs
    * Polygon.io API
    * OpenSea API (ty for the api ilysm)

## The why

cuz school made me think of project and i got hyperfixated

ew

## The contributing

Don't

## Roadmap

|        Feature         | Status | Latest Update | Release |
|:----------------------:|:------:|:-------------:|:-------:|
|        Basic UI        |   ✅    |  2022-02-25   |  %MVP   |
|   User Configuration   |   ✅    |  2022-03-19   |  %MVP   |
| API Connector: Finance |   🖥   |  2022-03-19   |  %MVP   |
|     Notifications      |   📅   |  2022-02-18   |  %MVP   |
|  API Connector: NFTs   |   📅   |  2022-02-18   |  %MVP   |

*Status legend: 📅 - Planned, 🖥 - In progress, ✅ - Done, 🚀 - Shipped*<br>
You Can Also track progress on [Gitlab issue tracker](https://gitlab.com/mineik/stonk-track/issues)

## Don't read this I put this here so I don't lose it (this is like browser favourites but worse)

* [ ] [The NFT API docs](https://docs.opensea.io/reference/api-overview)