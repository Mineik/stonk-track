package StonkTrack.ui.components;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.swing.*;

import StonkTrack.connector.httpResults.finance.Ticker;
import StonkTrack.connector.market.*;
import StonkTrack.ui.components.pages.common.ISaveTab;
import StonkTrack.ui.components.pages.common.WithClosableTabsPage;
import StonkTrack.ui.components.pages.market.*;
import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.icons.FlatSearchIcon;

import java.util.LinkedList;
import java.util.List;
import java.util.function.BiConsumer;

public class MarketTokensTab extends BaseTab implements WithClosableTabsPage, ISaveTab<Token, Ticker> {
	List<Token> tokens;
	EntityManager manager;
	JTabbedPane pane;

	@Override
	public JComponent getComponent() {
		var layout = new JTabbedPane(JTabbedPane.RIGHT);

		layout.addTab("Search", new FlatSearchIcon(), new StonkTrack.ui.components.pages.market.Search(this).getComponent());
		this.tokens.forEach(token -> {
			System.out.println(token);
			renderSaved(layout, token);
		});
		this.pane = layout;
		return layout;
	}

	private void renderSaved(JTabbedPane layout, Token token) {
		var tab = new SavedStock(token);

		layout.addTab(token.getToken(), tab.getComponent());
		var comp = ((JComponent) layout.getComponentAt(layout.getTabCount()-1));
		comp.putClientProperty(FlatClientProperties.TABBED_PANE_TAB_CLOSABLE, true);
		comp.putClientProperty(FlatClientProperties.TABBED_PANE_TAB_CLOSE_CALLBACK, (BiConsumer<JTabbedPane, Integer>) this::closeTab);
	}

	@Override
	public void loadTabData() {
		try{
			if(this.manager == null){
				var managerFactory = Persistence.createEntityManagerFactory("StonkTrack.connector.market");
				this.manager = managerFactory.createEntityManager();

			}
			var query = this.manager.createQuery("select t from Token t", Token.class);
			var rest = query.getResultList();
			System.out.printf("Získáno %d výsledků z databáze%n", rest.size());
			this.tokens.addAll(rest);
		}catch(Exception err){
			System.out.println("Error querying data");
			System.out.println(err.getMessage());
		}

	}

	@Override
	protected void objectSetup() {
		this.tokens = new LinkedList<>();
	}

	@Override
	public void closeTab(JTabbedPane pane, Integer index) {
		var title = pane.getTitleAt(index);

		var entity = this.manager.find(Token.class, title);
		this.manager.remove(entity);

		this.manager.getTransaction().begin();
		this.manager.flush();
		this.manager.getTransaction().commit();

		pane.removeTabAt(index);
	}

	public void saveTab(Ticker ticker){
		this.saveTab(ticker.toDbEntity());
	}

	public void saveTab(Token tickerToken){
		this.manager.persist(tickerToken);
		this.manager.getTransaction().begin();
		this.manager.flush();
		this.manager.getTransaction().commit();

		this.renderSaved(this.pane, tickerToken);
		this.pane.revalidate();
	}
}
