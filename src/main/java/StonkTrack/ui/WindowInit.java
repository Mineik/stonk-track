package StonkTrack.ui;
import javax.swing.*;
import java.awt.*;

public class WindowInit {
	public static JFrame windowStartup(String title, int width, int height, boolean primary){
		var frame = new JFrame();
		frame.setTitle(title);
		frame.setSize(width, height);
		frame.setDefaultCloseOperation(primary? JFrame.EXIT_ON_CLOSE:JFrame.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
		frame.setResizable(false);

		UIManager.put( "TabbedPane.closeHoverForeground", Color.red );
		UIManager.put( "TabbedPane.closePressedForeground", Color.red );
		UIManager.getLookAndFeelDefaults().put( "TabbedPane.closeHoverBackground", null );

		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(WindowInit.class.getResource("/Icon.png")));

		return frame;
	}

	public static JFrame windowStartup(String title, boolean primary){
		return windowStartup(title, 1280,720, primary);
	}
}
