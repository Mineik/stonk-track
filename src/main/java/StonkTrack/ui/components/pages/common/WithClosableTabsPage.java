package StonkTrack.ui.components.pages.common;

import javax.swing.*;

public interface WithClosableTabsPage {
	public void closeTab(JTabbedPane pane, Integer index);
}
