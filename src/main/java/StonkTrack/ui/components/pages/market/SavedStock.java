package StonkTrack.ui.components.pages.market;

import StonkTrack.connector.market.History;
import StonkTrack.connector.market.Token;
import StonkTrack.ui.components.pages.common.SavedArticle;
import StonkTrack.ui.components.pages.common.SavedPageCommon;

import javax.swing.*;

public class SavedStock extends SavedArticle implements SavedPageCommon {
	Token token;

	@Override
	public JComponent getComponent(){
		var layout = super.getComponent();
		layout.add(new JLabel(this.token != null?this.token.getTokenName():"This is saved page example"));
		return layout;
	}

	@Override
	public void loadTabData() {
		History[] history = null;
		if(this.token != null)
			history = this.token.getPriceHistory();
	}

	@Override
	protected void objectSetup() {

	}

	public SavedStock(Token token){
		super();
		this.token = token;
	}

	public SavedStock(){super();}
}
