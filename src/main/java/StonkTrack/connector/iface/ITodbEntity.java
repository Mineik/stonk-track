package StonkTrack.connector.iface;

public interface ITodbEntity<T> {
	public T toDbEntity();
}
