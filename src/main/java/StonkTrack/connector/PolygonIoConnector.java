package StonkTrack.connector;

import StonkTrack.connector.err.HttpRequestException;
import StonkTrack.connector.httpResults.finance.ClosePriceSymbol;
import StonkTrack.connector.httpResults.finance.Ticker;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

/**
 * Polygon.io connector for StonkTrack
 * just adapter for http requests
 *
 * @author Daniel Brada <danik.brada@seznam.cz>
 */
public class PolygonIoConnector extends HttpConnector {
	private final String BASE_URL;
	private final Properties properties;

	public PolygonIoConnector(String baseUrl) throws IOException {
		this.BASE_URL = baseUrl;

		this.properties = new Properties();
		this.properties.load(getClass().getClassLoader().getResourceAsStream("application.properties"));
	}

	/**
	 * Queries basic data about Ticker from Polygon API
	 *
	 * @param symbol Ticker code tracked by Polygon.io
	 * @return Ticker info about ticker
	 */
	public Ticker queryTickerInfo(String symbol) throws HttpRequestException {
		var r = this.createBaselineRequest()
				.uri(URI.create(String.format("%s/v3/reference/tickers/%s", this.BASE_URL, symbol)))
				.build();

		try {
			return super.onExecuteRequest(r, Ticker.class, (JsonObject obj) -> {
				if (!obj.has("count") && obj.has("results")) return obj.getAsJsonObject("results");
				return null;
			});
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Query all available historic closing prices for ticker from Polygon API
	 *
	 * @param symbol Ticker code tracked by Polygon.io
	 * @return Historic data for given symbol (past two years)
	 */
	public ClosePriceSymbol[] queryAllData(String symbol) throws HttpRequestException {
		var dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		var r = this.createBaselineRequest()
				.uri(URI.create(String.format("%s/v2/aggs/ticker/%s/range/1/day/%s/%s?adjusted=true&sort=asc&limit=731", this.BASE_URL, symbol, dateFormatter.format(LocalDate.now().minusDays(730)), dateFormatter.format(LocalDate.now()))))
				.build();

		try {
			return super.onExecuteRequest(r, ClosePriceSymbol[].class, (JsonObject obj) -> {
				if(obj.has("results")) return obj.getAsJsonArray("results");
				return null;
			});
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}


		return null;
	}


	/**
	 * TODO
	 *
	 * @param symbol
	 * @return latest close price for given symbol
	 */
	public ClosePriceSymbol queryPastDayData(String symbol) {

		return null;
	}

	@Override
	public HttpRequest.Builder createBaselineRequest() {
		var b = HttpRequest.newBuilder()
				.setHeader("Authorization", String.format("Bearer %s", this.properties.getProperty("polygon_io.api_key")));
		return super.addAppSpecificHeaders(b);
	}
}
