package StonkTrack.connector.iface;

/**
 *
 * @param <T> InsertData
 * @param <R> ReturnData
 */
public interface IRequestCallback<T,R> {
	public R item(T input);
}
