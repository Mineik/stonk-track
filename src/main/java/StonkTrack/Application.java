package StonkTrack;
import StonkTrack.connector.config.ConfigManager;
import StonkTrack.ui.*;
import com.formdev.flatlaf.FlatDarkLaf;

public class Application {
	public static void main(String[] args) {
		var config = new ConfigManager();
		// Setup application theme
		// Using the FlatLaf library, because ♥
		FlatDarkLaf.setup();

		var applicationWindow = WindowInit.windowStartup("StonkTrack", true);
		applicationWindow.add(ApplicationLayout.createMainApplicationFrameLayout());

	}
}
